﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    float idleTime = 0.1f;
    float timer = 0.0f;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    int count = 5;
    public Slider sliderExample;

    public Renderer renderer;
    public Material matJohnPBR;
    public Material matJohnToon;
    public Material matJohnTransparent; //references the material that John Lemon takes on when invisible

    public bool invulnerable;
    float invulnerableCooldown; //references the cooldown between invisibility uses

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        SetSlider();
    }

    void SetSlider()
    {
        sliderExample.value = count;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        bool isWalking = hasHorizontalInput || hasVerticalInput;
        if (!isWalking)
        {
            timer += Time.deltaTime;
            if(timer >= idleTime)
            {
                m_Animator.SetBool("IsWalking", false);
                timer = 0f;
            }
        } else
        {
            m_Animator.SetBool("IsWalking", true);
            timer = 0f;
        }
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward); 
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void Update()
    {
        if (invulnerable)
        {
            invulnerableCooldown -= Time.deltaTime;
            print("invulnerabilityCooldown:" + invulnerableCooldown);
            if (invulnerableCooldown <= 0f)
            {
                invulnerable = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && invulnerableCooldown <= 0f)
        {
            invulnerable = true;
            invulnerableCooldown = 5f;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Material[] mats = new Material[] { matJohnTransparent, matJohnTransparent };
            renderer.materials = mats;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Material[] mats = new Material[] { matJohnToon, matJohnPBR };
            renderer.materials = mats;
        }

        if (Input.GetKeyDown(KeyCode.Space))
            {
                if (count > 0)
                    count--;
                SetSlider(); // Updates the slider as the powerup is used/spacebar is pressed.
            }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (count >= 5)
                invulnerableCooldown = 14000f; //Upon being used 5 times, the invulnerability cooldown extends to a ridiculous amount. Any players who try to use the powerup after the slider explicitly tells them they can't use it anymore, will be punished!
        }
    }
}
